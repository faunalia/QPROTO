# QProto Processing Algorithm

QPROTO is an algorithm for rockfall analyses at a small scale on the basis of
the simplified *Cone Method* (Jaboyedoff & Labious, 2011). 

It allows to preliminarily evaluate the effects of a rockfall event along a
slope in terms of exposed areas, boulder velocity and boulder energy.

A time-independent rockfall hazard can also be defined.

QPROTO is designed by Politecnico di Torino (Marta Castelli, Monica Barbero &
Marco Grisolia), Arpa Piemonte (Rocco Pispico & Luca Lanteri) and developed by
Faunalia.

This Plugin is partially funded by Italian 2015 PRIN Project: "Innovative
monitoring and design strategies for sustainable landslide risk mitigation".

WARNING
Due to evolutions of the SAGA modules used in the plugin, QPROTO is not working in the latest versions of QGIS.
While waiting to update the plugin, we recommend using the portable version "QGIS 3.16.6-1 Hannover" which can be used safely without interference with other QGIS installations.
In this repository by Salvatore Fiandaca [1] you can download QGIS version 3.16.6-1 Hannover Portable Grass 7.8.5 LTR [2] and then install QPROTO.
[1] https://github.com/pigreco/QGIS_portable_3x
[2] https://drive.google.com/file/d/1em9phgWHZd_gDM2ItiliYWBOEFMx1rOS/view?usp=sharing
